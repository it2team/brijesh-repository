import requests
from bs4 import BeautifulSoup

url = "https://www.amazon.in/?&ext_vrnc=hi&tag=googhydrabk1-21&ref=pd_sl_7hz2t19t5c_e&adgrpid=58355126069&hvpone=&hvptwo=&hvadid=486458712209&hvpos=&hvnetw=g&hvrand=532859894982519243&hvqmt=e&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9062191&hvtargid=kwd-10573980&hydadcr=14453_2154373&gclid=Cj0KCQiA3rKQBhCNARIsACUEW_ZSRcAI4aH6ctmWgNIL8zjiel3duK36aWyCzpjXwnpf4nmRoP6IB3IaAjoBEALw_wcB"
r = requests.get(url)

soup = BeautifulSoup(r.text,'html.parser')

print(soup.title.text)

images = soup.find_all("img")

print(images[0])

for image in images:
    alt = image['alt']
    src = image['src']
    print(alt,"\n")
    with open(alt.replace(' ','_').replace('/','').replace('.','_').replace(':',"_").replace(',','_').replace('%','_').replace('|','_') +'.jpg','wb') as f:
        im = requests.get(src)
        f.write(im.content)


# ===========================images download from websites using web scrapping===================