
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useSelector,useDispatch } from 'react-redux';
import { increment, decrement ,incrementByAmount} from './counterSilce'
const Counter =() => {
    const count=useSelector((state)=>state.counter.count)
    const dispatch = useDispatch();

    return (
        <div>
                <span className='value'>{count}</span>  <br />
                <AddCircleIcon onClick={()=>{dispatch(increment())}} />
                <RemoveCircleIcon onClick={()=>{dispatch(decrement())}} /><br />
                <button onClick={()=>{dispatch(incrementByAmount(10))}}>increment By 10</button>
        </div>
    )
}

export default Counter;
