import { useState } from "react"
import { useDispatch } from 'react-redux';
import { changeTheColor } from "./ThemeSlice";
const Theme = () =>{
    const [color,setColor] = useState();
    const dispatch = useDispatch();
    return(
        <>
            <input name="color" placeholder="Enter the color here" onChange={(e)=>setColor(e.target.value)}></input>
            <button onClick={()=>{
                dispatch(changeTheColor(color))
            }}>change Color</button>
        </>
    )
}
export default Theme;