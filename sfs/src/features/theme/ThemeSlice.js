import { createSlice } from '@reduxjs/toolkit'

const initialColorValue = {
  color:'red',
}

export const colorSlice = createSlice({
  name: 'color',
  initialState:initialColorValue,
  reducers: {
    changeTheColor: (state, action) => {
      state.color= action.payload
    },
  },
})  

export const { changeTheColor } = colorSlice.actions

export default colorSlice.reducer

