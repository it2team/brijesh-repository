// import Coin from './features/coin/Coin'
import './App.css';
import Counter from './features/counter/Counter.js';
import Coin from './features/coin/Coin'
import Theme from './features/theme/Theme'
const App =() =>{
  return(
    <>
    <div className='main'>
      <Counter />
      <Coin />
      <Theme />
    </div>
    </>
  )
}
export default App;
