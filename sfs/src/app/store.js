import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSilce";
import changeTheColorReducer from '../features/theme/ThemeSlice'
//configureStore AUTOMATICALLY CONFIGURE DEVTOOL
export const store = configureStore({
    reducer:{
        counter:counterReducer,  //this name and the couter component state.counter.count is related
        theme: changeTheColorReducer
    },
})