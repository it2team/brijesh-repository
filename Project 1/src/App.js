import Sidebar from './Components/Sidebar';
import './App.css';
import Header from './Components/Header';
import {Routes,Route} from 'react-router-dom';

import Company from './Components/Pages/Brands/Company'; 
import Category from './Components/Pages/Category/Category';
import Car from './Components/Pages/Car/Car';
// import Customer from './Components/Pages/Customer/Customer';
// import TestDrive from './Components/Pages/Testdrive/TestDrive';

import CompanyEdit from './Components/Pages/Brands/CompanyEdit';
import CompanyAdd from './Components/Pages/Brands/CompanyAdd';

import CategoryAdd from './Components/Pages/Category/Categoryadd';
import CategoryEdit from './Components/Pages/Category/Categoryedit';

// import CarAdd from './Components/Pages/Car/CarAdd';
import CarEdit from './Components/Pages/Car/CarEdit';
import CarAdd from './Components/Pages/Car/CarAdd';
const App = () => {
  return (
    <>
      <Sidebar />
      <Header />
      <div className = 'main'>
        <Routes>
            <Route path='/brand' element={<Company />} />
            <Route path='/brand/add' element={<CompanyAdd />} />
            <Route path='/brand/edit/:id' element={<CompanyEdit />} />


            <Route path='/category' element={<Category />} /> 
            <Route path='/category/add' element={<CategoryAdd />} />
            <Route path='/category/edit/:id' element={<CategoryEdit />}/>


            <Route path='/car' element={<Car />} />
            <Route path='/car/add' element={<CarAdd />} />
            <Route path='/car/edit' element={<CarEdit />} />


            {/* <Route path='/customer' element={<Customer />} /> */}
            {/* <Route path='/category/categoryadd' element={<CustomerAdd />} />
            <Route path='/category/categoryedit' element={<CustoomerEdit />} /> */}
{/* 

            <Route path='/testDrive' element={<TestDrive />} />
            <Route path='/category/categoryadd' element={<Companyadd />} />
            <Route path='/category/categoryedit' element={<Companyedit />} /> */}
            
        </Routes>
      </div> 
    </>
  );
}

export default App;
