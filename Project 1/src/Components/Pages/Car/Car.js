import "./Car.css";
import axios from "axios";
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
} from "@material-ui/core";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

const Car = () => {
  const [car, setCar] = useState([]);
  useEffect(() => {
    getDetails();
  }, []);

  async function getDetails() {
    try {
      const car = await axios.get(`http://103.254.172.218:4200/addcar`);
      // console.log(company.data);
      setCar(car.data);
    } catch (error) {
      console.log("Something is Wrong");
    }
  }

  const handleDelete = async (id) => {
    await axios.delete("http://103.254.172.218:4200/addcar", {
      data: { car_id: id },
    });
    var newCar = car.filter((item) => {
      // console.log(item);
      return item.id !== id;
     })
     setCar(newCar);
  };

  return (
    <>
      <div className='car'>
        <div className='hiii'>
          <ul className='car_list'>
            <li>
              <div className='title_car'>
                <p>Car Brands</p>
              </div>
            </li>
            <li>
              <a href='car/add'>Add New</a>
            </li>
          </ul>
        </div>
        <div>
          <TableContainer>
            <Table>
              <TableHead className='column'>
                <TableRow>
                  <TableCell><span>Index</span></TableCell>
                  <TableCell><span>Car Id</span></TableCell>
                  <TableCell><span>Car Name</span></TableCell>
                  <TableCell><span>Mileage</span></TableCell>
                  <TableCell><span>Company Name</span></TableCell>
                  <TableCell><span>Category Name</span></TableCell>
                  <TableCell><span>Colour</span></TableCell>
                  <TableCell><span>Engine Type</span></TableCell>
                  <TableCell><span>Fuel</span></TableCell>
                  <TableCell><span>Image</span></TableCell>
                  <TableCell><span>Price</span></TableCell>
                  <TableCell><span>Transmission</span></TableCell>
                  <TableCell><span>Action</span></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {car.map((car,i) => {
                  return (
                    <TableRow>
                      <TableCell>{i+1}</TableCell>
                      <TableCell>{car.car_id}</TableCell>
                      <TableCell>{car.Car_Name}</TableCell>
                      <TableCell>{car.car_mileage}</TableCell>
                      <TableCell>{car.car_companyName}</TableCell>
                      <TableCell>{car.category_name}</TableCell>
                      <TableCell>{car.colour}</TableCell>
                      <TableCell>{car.engine_type}</TableCell>
                      <TableCell>{car.fuel_type}</TableCell>
                      <TableCell>
                          <img src={car.image} alt='wait for moment' height='100px' width='100px'></img>
                      </TableCell>
                      <TableCell>{car.price}</TableCell>
                      <TableCell>{car.transmission}</TableCell>
                      
                      <TableCell><IconButton>
                          <Link to={`/car/edit/${car.car_id}`}><EditIcon /></Link>
                      </IconButton>
                      <IconButton onClick={(e) => handleDelete(car.car_id)}>
                          <DeleteIcon />
                      </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    </>
  );
};

export default Car;