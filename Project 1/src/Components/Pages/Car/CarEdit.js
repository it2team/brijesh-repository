import { Link } from "react-router-dom";
import Files from "react-files";


const CarEdit = () =>{
    return(
        <>
        <div className='category'>
        <ul className='category_list'>
          <li>
            <div className='title_category'>
              <p>Update Car</p>
            </div>
          </li>
          {/* <li>
            <Link to={`/category/add`}>Add New</Link>
          </li> */}
          <li>
            <Link to='/category'>Back TO Home</Link>
          </li>
        </ul>
      </div>
      <div className='carcompany_add_form'>
        <form>
          <div className='form'>
            <p>Enter the Car Name</p>
            <input
              type='name'
            //   onChange={(e) => changeHandler(e)}
              placeholder='Enter the Brand Name'
              name='car_companyName'></input>
            {/* <p>Enter the Date</p>
            <input
              type='date'
              onChange={(e) => changeHandler(e)}
              placeholder='ENter the Date'
              name='date'></input>
            <p>Choose the Status</p>
            <select name='status' value='1' onChange={changeHandler}>
              <option label='Active' value='1'>
                Active
              </option>
              <option label='Off' value='0'>
                Off
              </option>
            </select> */}
            <p>Upload Logo</p>
            <div className='logo'>
              <Files
                // onChange={(e) => uploadHandler(e)}
                accepts={["image/*"]}
                maxFiles={1}
                name='car_companyLogo'
                maxFileSize={10000000}
                minFileSize={0}
                clickable>
                <p className="upd">Hello</p>
              </Files>
            </div>
            <div className="img">
              <img alt="Wait" height='100px' width='100px'></img>
            </div>
            {/* <button onClick={(e) => onSubmit(e)} className='but_cta'>
              Submit
            </button> */}
            <hr />
          </div>
        </form>
      </div>
      
      
      </>
      

    );
}

export default CarEdit;