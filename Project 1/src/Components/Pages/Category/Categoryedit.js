import axios from "axios";
import "./Category.css";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
// import { useNavigate } from "react-router-dom";
const CategoryEdit = () => {
  const { id } = useParams();
  const [category, setCategory] = useState({
    category_name: "",
    date: "",
    status: "",
  });
  // const navigate= useNavigate();
  useEffect(() => {
    // console.log("inside the useEffect");
    getDetails(id);
  }, [id]);

  async function getDetails(id) {
    axios
      .get(`http://103.254.172.218:4200/singlecarcategory?cat_id=${id}`)
      .then((res) => {
        setCategory(res.data[0]);
      })
      .catch(() => {
        console.log("Something is Wrong");
      });
    // , {
    //   params: {
    //     cat_id: id,
    //   },
    // }
  }

  const textChangeHandler = (e) => {
    setCategory({
      ...category,
      [e.target.name]: e.target.value,
    });
    console.log(category)
  };

  // for post the updated data into the database
  async function onSubmit(e) {
    e.preventDefault();
    try {
      await axios.put(
        `http://103.254.172.218:4200/carcategory?cat_id=${id}`,
        category
      );
    } catch (error) {
      console.log("Something is Wrong");
    }
  }

  return (
    <>
      <div className='category'>
        <ul className='category_list'>
          <li>
            <div className='title_category'>
              <p>Update Category</p>
            </div>
          </li>
          {/* <li>
            <Link to={`/category/add`}>Add New</Link>
          </li> */}
          <li>
            <Link to='/category'>Back TO Home</Link>
          </li>
        </ul>
      </div>

      <div className='carcategory_add_form'>
        <form>
          <div className='form'>
            <input type='hidden' name='cat_id' value={id}></input>
            <p>Enter the Category Name</p>
            <input
              type='name'
              name='category_name'
              value={category.category_name}
              onChange={textChangeHandler}></input>
            {/* <p>Enter the Date</p>
            <input
              type='date'
              name='date'
              value={category.category_date}
              onChange={textChangeHandler}></input> */}
            <p>Set Status</p>
              
            <select name='status' onChange={textChangeHandler}>
                <option label="Active" value="1">Active</option>
                <option label="Off" value="1">Off</option>
            </select>

            {/* <input
              type='number'
              name='status'
              value={category.status}
              onChange={textChangeHandler}></input> */}
              <br />
            <button className='but_cta' onClick={onSubmit}>
              Submit
            </button>
          </div>
          
        </form>
      </div>
    </>
  );
};

export default CategoryEdit;
