import { useState } from "react";
import axios from "axios";
import "./Category.css";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
const Categoryadd = () => {
  const [category, setcategory] = useState({
    name: "",
    date: "",
    status: "",
  });
  const navigate= useNavigate();
  const textChangeHandler = (e) => {
    setcategory({
      ...category,
      [e.target.name]: e.target.value,
    });
  };

  async function onSubmit(e) {
    e.preventDefault();
    try {
      await axios.post(`http://103.254.172.218:4200/carcategory`, category);
      navigate.push('/category');
    } catch (error) {
      console.log("Something is Wrong");
    }
  }
//   function handleClick(){
//       navigate.push("category")
//   }
  return (
    <>
      <div className='category'>
        <ul className='category_list'>
          <li>
            <div className='title_category'>
              <p>Add New Category</p>
            </div>
          </li>
          <li>
            <Link to={`/category/add`}>Add New</Link>
          </li>
        </ul>
      </div>

      <div className='carcategory_add_form'>
        <form>
          <div className='form'>
            <p>Enter the Category name</p>
            <input
              type='name'
              onChange={(e) => textChangeHandler(e)}
              placeholder='ENter the Category Name'
              name='category_name'></input>
            <p>Enter the Date</p>
            <input
              type='date'
              onChange={(e) => textChangeHandler(e)}
              placeholder='ENter the Date'
              name='date'></input>
            <p>Enter the Status</p>
            <select name='status' onChange={textChangeHandler} disabled>
                <option label="Active" value="1">Active</option>
                <option label="Off" value="1">Off</option>
            </select>
            {/* <input
              type='number'
              onChange={(e) => textChangeHandler(e)}
              placeholder='ENter the status'
              name='status'
              value='1'
              disabled></input> */}
            <hr />
            <button onClick={(e) => onSubmit(e)} className='but_cta'>
              Submit
            </button>
            <hr />
          </div>

          <Link to='/category'>Back TO Home</Link>
        </form>
      </div>
    </>
  );
};

export default Categoryadd;
