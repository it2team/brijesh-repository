import './Category.css';
import axios from 'axios';
import { TableContainer, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core"
import {useEffect, useState} from 'react'; 
import { useParams } from 'react-router-dom';
const CategoryView = () =>{
    

    const { cat_id } = useParams();
    const [category, setcategory] = useState([]);
    // const history = useHistory();
    useEffect(() => {
        console.log("catid" ,cat_id)
        async function getcategory() {
        try {
            const category = await axios.get(`http://103.254.172.218:4200/carcategory/${cat_id}`)
            console.log(category.data);

            setcategory(category.data);
        } catch (error) {
            console.log("Something is Wrong");
        }
    }

    getcategory();
    }, [cat_id])
    // function handleClick() {
    //     history.push("/")
    // }


    return(
        <>
            <div className='category'>
   
                <ul className='category_list'>
                <li>
                    <div className="title_category"><p>Category View</p></div>
                </li>
      
            </ul>
        <div>
        <TableContainer>
            <Table>
                <TableHead className='column'>  
                    <TableRow>
                        <TableCell><span>CategoryId</span></TableCell>
                        <TableCell><span>CategoryName</span></TableCell>
                        <TableCell><span>Date</span></TableCell>
                        <TableCell><span>Status</span></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {category.map((category,i)=>
                    {
                        return(
                            <TableRow key={i}>
                                <TableCell>{category.cat_id}</TableCell>
                                <TableCell>{category.category_name}</TableCell>
                                <TableCell>{category.date}</TableCell>
                                <TableCell>{category.status}</TableCell>
                                {/* <div>
                                    <TableCell><IconButton><Link to={`/view/${category.cat_id}`}><EditIcon /></Link></IconButton></TableCell>
                                    <TableCell><IconButton onClick={()=>handleDelete(category.cat_id)}><DeleteIcon /></IconButton></TableCell>
                                </div> */}
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
        </div>
                    
        </div>
        </>

    );
}
export default CategoryView;