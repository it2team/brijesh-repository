import './Register.css';
import { auth } from '../../firebase';
import { useState } from 'react';
import {toast,toastContainer} from 'react-toastify'
const Register = () =>{
    const [email,setEmail] = useState('');

    const handleSubmit = () =>{
        //
    }
    const RegisterForm = () => {
        return(
            <>
                <form onSubmit={handleSubmit}>
                    <input type="email" className="form-control" value={email} onChange={(e) =>{setEmail(e.target.value)}} autoFocus/>
                    <button type="submit" className='btn btn-raised'>Register</button>
                </form>
            </>

        )
 
    }
    return(
        <>
            <div className="container">
                <h4>Register</h4>
                {RegisterForm()}
            </div>
        </>
    );
};

export default Register;