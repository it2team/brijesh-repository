import Login from "./Pages/Auth/Login";
import Register from "./Pages/Auth/Register";
import Home from "./Home";
import {Route,Routes} from 'react-router-dom';
import Header from "./Components/Nav/Header";
const App = () =>{
  return(
    <>
    <Header /> 
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Register' element={<Register />} />
        <Route path='/Login' element={<Login />} />
      </Routes>
    </>
  )
}
export default App;
