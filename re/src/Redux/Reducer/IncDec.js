
//set the value of initial set
const initialState = 0;

//reducer which can search for which case to excute
const changeTheNumber = (state=initialState,action) =>{
    switch(action.type){
        case "INCREMENT":
            return state+action.num;
        case "DECREMENT":
            return state-1;
        default:return state;
    }
}

export default changeTheNumber;