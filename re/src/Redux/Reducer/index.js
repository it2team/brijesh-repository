import changeTheNumber from "./IncDec";
import { combineReducers } from "redux";


//when multiple reducer take place in application we need to combine them into one root reducers
const rootReducer = combineReducers({
    changeTheNumber
})

export default rootReducer;