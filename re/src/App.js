import {incNumber,decNumber} from './Redux/Action/index'
import { useSelector,useDispatch } from "react-redux";
const App = () =>{

    //Allows us to extract data from the redux store state its like consumer
    const myState = useSelector((state)=>state.changeTheNumber)
    console.log(myState,"_________")
    //for fetch the action which can we require
    const dispatch = useDispatch();
    return(
        <>  
        <div>
            <h1>Increment Decrement</h1>
            <input name="input" type="text" value={myState} />
            <button onClick={() => dispatch(incNumber(5))}>Increament</button>
            <button onClick={() => dispatch(decNumber())}>Decreament</button>
        </div>
            
        </>
    );
}
export default App;