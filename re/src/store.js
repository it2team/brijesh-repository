import {createStore} from 'redux';
import rootReducer from "./Redux/Reducer/index";

//it creates a store and set root Reducer as a store
const store = createStore(rootReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;   