import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import store from "./store";
import { Provider } from "react-redux";
        
store.subscribe(() => console.log(store.getState()));
ReactDOM.render(

  // provide the store to the all children components
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
