from flask_restful import Resource
from model import itemModel
from flask_restful import Resource, reqparse

class getItemCategory(Resource):
    def get(self):
        try:
            # parser = reqparse.RequestParser()
            data=itemModel.itemCategoryModel.getItemCategoryData()
            print("resourse================",data)
            # if data.status_code == 400:
            #     return {'message':"No data Found"},400
            return data
        except Exception as e:
            print("error",e)

    def post(self):
        try:

            parser = reqparse.RequestParser()
            # parser.add_argument('itemCategoryId',type=str,required=True,help="itemCategoryId is required")
            parser.add_argument('itemCategoryName',type=str,required=True,help="itemCategoryName is required")
            parser.add_argument('status',type=int,required=True,help="status is required")
            parser.add_argument('dateAdded',type=str,required=True,help="dateAdded is required")
            args = parser.parse_args()
            print("+===================",args)
            data=itemModel.itemCategoryModel.postItemCategoryData(args)
            print("=================================",data)
            return data
        except Exception as e:
            return {'message':e},400
        


