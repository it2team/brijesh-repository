from flask import Flask, jsonify, request
from flask_restful import Api 
# creating a Flask app
from db import mysql

from resource.itemResource import *
app = Flask(__name__)

# =================================================
# @app.route('/', methods = ['GET', 'POST'])
# def home():
#     if(request.method == 'GET'):
  
#         data = "hello world"
#         return jsonify({'data': data})
#  =============================================== 

# MySQL configurations

api =Api(app)
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'mysql1234'
app.config['MYSQL_DATABASE_DB'] = 'sunled'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

api.add_resource(getItemCategory,'/getItemCategory')
# api.add_resource(getItemCategory,'/addItemCategory')

# driver function
if __name__ == '__main__':
    app.run(host="192.168.0.110",port=4000,debug = True)