from ast import Return
import pymysql
from db import mysql
from flask import jsonify
from datetime import datetime
class itemCategoryModel:
            
    @classmethod
    def getItemCategoryData(self): #to check if item Category is available or not 
        try:
            print("trymodal ===============================")
        
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                sql = "select * from Item_Category where itemCategoryId=31 order by itemCategoryId desc"                            
                cursor.execute(sql)
                data = cursor.fetchall()
            except Exception as e:
                print("sql ===sql==============",e)
                # return {"message":" error in sql"},400
            finally:
                cursor.close()
                conn.close()
            
            # lst=[]           
            # for i in data:      
            #     i['dateAdded']=str(i['dateAdded'])
            #     lst.append(i)
            # return(lst)

            # if len(lst)<=0:
            #     return self.not_found()
            # for i in range(len(data)):
                # print("\n",data[i])
            lst=[]
            if len(data) > 0:
                print(data)
                for i in data:
                    print(i['dateAdded'])      
                    # i['dateAdded'] = datetime.struct_time((d.year, d.month, d.day))
                    # d = datetime.fromisoformat(i['dateAdded'].date)                        
                    i['dateAdded'] = str(i['dateAdded'].strftime("%Y-%m-%d %S"))               
                    # i['dateAdded']=str(i['dateAdded'])   
                    lst.append(i)
             
            if len(lst)<=0:
                return {"message":"Data Not Found"},400

            return lst,200
       
        except Exception as e:
            return {"message" : "error message in item Model File"}
            # print("",e)
            # print(e)

    @classmethod
    def postItemCategoryData(self,args):
            print(" ===============================",args,"==========================================")
        
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                sql =  """INSERT INTO Item_Category (itemCategoryName,status,dateAdded)
                     VALUES (%s,%s,%s);""" 
                a = (args["itemCategoryName"],args["status"],args["dateAdded"])
                print(" ===============================",a,"==========================================")
                cursor.execute(sql,a)
                conn.commit()
                # data = cursor.fetchall()
                res = jsonify({'message':'Item Category is added successfully'})
                return res
            except Exception as e:
                print("sql ===sql==============",e)
                
                return {"message":" error in sql"},400
            finally:
                cursor.close()
                conn.close()

        # @classmethod
        # def addCar(self, args):
        # try:
        #     con = mysql.connect()
        #     cursor = con.cursor(pymysql.cursors.DictCursor)

        #     sql = """ INSERT INTO AddCar (carmela_id,company_name,car_category,price,
        #               car_mileage,engine_type,transmission,fuel_type,seater,colour,image) 
        #               VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s); """
           
        #     data = (args["carmela_id"],args["company_name"],args["car_category"],args["price"],
        #             args["car_mileage"],args["engine_type"],args["transmission"],args["fuel_type"],
        #             args["seater"],args["colour"],args["image"])
            
        #     cursor.execute(sql,data)
        #     # cursor.execute(sql1,data1)
        #     con.commit()
        #     # id =args["emp_id"]
        #     cursor.close()
        #     con.close()
        #     lst = []
        #     res = jsonify({'message':'Car Details Is Added Successfully'})
        #     res.status_code = 200
        #     return res

        # except Exception as e:
        #     print("err", e)
        #     return {"message": "Error in Api"}, 500