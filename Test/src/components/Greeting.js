import {useState} from 'react';
import Output from './Output';
const Greeting = () =>{
    const [changedText, setChangedText] = useState(false);
    const textchangehandler = () =>{
        setChangedText(true);
    }
    return(
        <>
            <h2>Hello world</h2>
            {
                !changedText && <Output>Not Changed</Output>
            }
            {
                changedText && <Output>Changed</Output>
            }
            <button onClick={textchangehandler}>Change Text!</button>
        </>
    )
}

export default Greeting;