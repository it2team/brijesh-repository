import { render,screen } from "@testing-library/react";
import Aysnc from './Async';
describe('Async Component',()=>{
    test('renders posts if request succeeds',async()=>{
        window.fetch = jest.fn();
        window.fetch.mockResolvedValueOnce({
            json: async () =>[{id:'p1',title:'First post'}],
        });
        render(<Aysnc />)

        const listItemElements = await screen.findAllByRole('listitem');
        expect(listItemElements).not.toHaveLength(0);
    });
})