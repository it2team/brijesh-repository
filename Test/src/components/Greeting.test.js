import { render, screen } from "@testing-library/react";
import Greeting from "./Greeting";
import userEvent from '@testing-library/user-event'
describe("Greeting Component", () => {
  test("renders Hello World as a text", () => {
    //arrange
    render(<Greeting />);

    //act

    //assert
    const helloWorldElement = screen.getByText("Hello World", { exact: false });
    expect(helloWorldElement).toBeInTheDocument();
  });

  test("renders button not clicked", () => {
    render(<Greeting />);
    const outputElement = screen.getByText("Not Changed");
    expect(outputElement).toBeInTheDocument();
  });

  test("renders button clicked", () => {
    render(<Greeting />);

    //act
    const buttonElement = screen.getByRole('button');
    userEvent.click(buttonElement)

    //Assert
    const outputElement1 = screen.getByText("Changed");
    expect(outputElement1).toBeInTheDocument();
  });
});
