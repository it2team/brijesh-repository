import { Routes, Route } from 'react-router-dom';
import AuthContext from './store/auth-context';
import Layout from './components/Layout/Layout';
import UserProfile from './components/Profile/UserProfile';
import AuthPage from './pages/AuthPage';
import HomePage from './pages/HomePage';
import { useContext } from 'react';


const App = () => {

  const authCtx= useContext(AuthContext)
  return (
    <Layout>
      <Routes>
        <Route path='/' element={ <HomePage />} />
        
        {/* if the user can not login means that the isLoggedIn set as false then the page is redirect to the AuthPage  */}
        {
          !authCtx.isLoggedIn && (<Route path='/auth' element={<AuthPage />} />)
        }
        
        {/* if the user can login means that the isLoggedIn set as True then the page is redirect to the UserProfile */}
        {
          authCtx.isLoggedIn && (<Route path='/profile' element={<UserProfile />} />) 
        }
        

        {/* any other things can be write in url by the user then it can be navigate to the auth page for the login */}
        <Route path='*' element={<HomePage />} />

      </Routes>
    </Layout>
  );
}

export default App;
