import classes from './ProfileForm.module.css';
import { useRef,useContext} from 'react';
import AuthContext from '../../store/auth-context';
import { useNavigate } from 'react-router-dom';
const ProfileForm = () => {
  const navigate = useNavigate();
  const newPasswordInputRef = useRef(); 
  const authCtx =  useContext(AuthContext)
  const submitHandler = event =>{
    event.preventDefault();
    const enteredNewPassword = newPasswordInputRef.current.value;


    //add validation
    fetch('https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyDoNusBgxP6wN8hH2kp60MwuaAxHO-8Dos',{
          method: "POST",
          body: JSON.stringify({
            idToken:authCtx.token,
            password:enteredNewPassword,
            returnSecureToken:false
           }),
          headers: {
            'Content-Type': 'application/json'
          }
    }).then((res )=>{
        alert("password changed successfully")
        navigate('/')
    })
  }
  
  return (
    <form onSubmit={submitHandler} className={classes.form}>
      <div className={classes.control}>
        <label htmlFor='new-password'>New Password</label>
        <input type='password' id='new-password' minLength="8" ref={newPasswordInputRef}/>
      </div>
      <div className={classes.action}>
        <button>Change Password</button>
      </div>
    </form>
  );
}

export default ProfileForm;
