 import { useState, useRef ,useContext} from "react";
import { useNavigate } from 'react-router-dom';
// import axios from 'axios';
import classes from "./AuthForm.module.css";
import AuthContext from "../../store/auth-context";
const AuthForm = () => {
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const authCtx = useContext(AuthContext);
  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    const enteredEmail = emailInputRef.current.value;
    const enteredpassword = passwordInputRef.current.value;

    setIsLoading(true);
    let url;
    if (isLogin) {
      
      url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDoNusBgxP6wN8hH2kp60MwuaAxHO-8Dos'
    } else {
      url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDoNusBgxP6wN8hH2kp60MwuaAxHO-8Dos`
    } 
      fetch(
        url,
        {
          method: "POST",
          body: JSON.stringify({
            email: enteredEmail,
            password: enteredpassword,
            returnSecureToken: true,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      ).then((res) => {
        setIsLoading(false);
        if (res.ok) {
          return res.json();
        } else {
          return res.json().then((data) => {
            let errorMessage = "Enter the correct credentials otherwise Sigh up First";
            // if (data && data.error && data.error.message) {
            //   errorMessage = data.error.message;
            // }
            
            throw new Error(errorMessage);
          });
        }
      }).then((data) =>{

        // const expirationTime = new Date(new Date().getTime() +(+data.expireIn*1000));
        authCtx.login(data.idToken) 
        //  expirationTime.toISOString());
        navigate('/');
        // console.log(data)
      })
      .catch((err) => {
        alert(err.message);
      });

  };  

  // const fd = {
  //   email:enteredEmail,
  //   password:enteredpassword,
  //   returnSecureToken:true
  // }
  // try {
  //   await axios.post(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDoNusBgxP6wN8hH2kp60MwuaAxHO-8Dos`,fd);
  //   console.log("success");
  // } catch (error) {
  //   console.log("Something is Wrong");
  // }

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? "Login" : "Sign Up"}</h1>
      <form onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor='email'>Your Email</label>
          <input type='email' id='email' required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor='password'>Your Password</label>
          <input
            type='password'
            id='password'
            required
            ref={passwordInputRef}
          />
        </div>
        <div className={classes.actions}>
          {!isLoading && <button>{isLogin ? "Login" : "Create Account"}</button>} 
          {isLoading && <p>Sending Request...</p>}
          
          <button
            type='submit'
            className={classes.toggle}
            onClick={switchAuthModeHandler}>
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
