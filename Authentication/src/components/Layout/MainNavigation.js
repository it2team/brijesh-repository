import { Link,useNavigate  } from 'react-router-dom';
import { useContext } from 'react';
import AuthContext from '../../store/auth-context';
import classes from './MainNavigation.module.css';


//this page contain navigation 
const MainNavigation = () => {
  //here we use context for take access of the element of useContext from the auth-context
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;
  const navigate = useNavigate();
  
  const logoutHandler = () =>{
    authCtx.logout();
    navigate('/');
  }

  return (
    <header className={classes.header}>
      <Link to='/'>
        <div className={classes.logo}>Authentication App</div>
      </Link>
      <nav>
        <ul>

        {/* here we give the condition if the user can loggin then only profile,service and logout button can be visible otherwise it can be Only visible login button */}
        {isLoggedIn &&<li><p>UserName</p></li>}


        {!isLoggedIn &&(<li>
            <Link to='/auth'>Login</Link>
          </li>)}

        {isLoggedIn && (<li>
            <Link to='/profile'>Profile</Link>
          </li>   
        )}

        {isLoggedIn && (<li>
            <Link to='/services'>Service</Link>
          </li>   
        )}

        {isLoggedIn && (<li>
            <button onClick={logoutHandler}>Logout</button>
          </li>
        )}
          
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
