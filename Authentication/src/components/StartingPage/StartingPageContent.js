import classes from './StartingPageContent.module.css';


//this is a homepage of the website
const StartingPageContent = () => {
  return (
    <section className={classes.starting}>
      <h1>HomePage</h1>
    </section>
  );
};

export default StartingPageContent;
