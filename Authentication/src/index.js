import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './App';
import {AuthContextProvider} from './store/auth-context'
ReactDOM.render(
  // authcontext provide to the App component
  <AuthContextProvider>  
    <BrowserRouter>
    <App />
  </BrowserRouter>
  </AuthContextProvider>,
  document.getElementById('root')
);
