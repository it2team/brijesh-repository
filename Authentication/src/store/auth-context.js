import React, { useState } from "react";

const AuthContext = React.createContext({
  token: "",
  isLoggedIn: false,
  login: (token) => {},
  logout: () => {}
});

export const AuthContextProvider = (props) => {
  const initialToken = localStorage.getItem('token')
    const [token, setToken] = useState(initialToken)

  const userIsLoggedIn = !!token; 
  //not understandable
  
  const logoutHandler = () => {
    setToken(null);
    // localStorage.removeItem('token');  
      //for removing token of current user from the local storage
  };

  const loginHandler = (token,expirationTime) => {
    setToken(token);
    // localStorage.setItem('token',token);

    // const remainingTime = calculateRemainingTime(expirationTime);
    // setTimeout(logoutHandler, remainingTime);
  };

  const contextValue = {
    token: token,
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
  };

  //this function is for the calculating remaining time for the for the user when the remaining time is over it can be automatically logout
  // const calculateRemainingTime = (expirationTime) => {
  //     const currentTime = new Date().getTime();
  //     const adjExpirationTime = new Date(expirationTime).getTIme;

  //     const remainingDuration = adjExpirationTime - currentTime;
  //     return remainingDuration;   
  // }
  return (

    //it provides the props to the children here it can be provide to the app components
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
