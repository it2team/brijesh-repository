import "./Company.css";
import axios from "axios";
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
} from "@material-ui/core";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

const Company = () => {
  const [company, setcompany] = useState([]);
  useEffect(() => {
    getDetails();
  }, []);

  async function getDetails() {
    try {
      const company = await axios.get(`http://103.254.172.218:4200/carcompany`);
      // console.log(company.data);
      setcompany(company.data);
    } catch (error) {
      console.log("Something is Wrong");
    }
  }

  const handleDelete = async (id) => {
    await axios.delete("http://103.254.172.218:4200/carcompany", {
      data: { carcompany_id: id },
    });
    var newcompany = company.filter((item) => {
      // console.log(item);
      return item.id !== id;
     })
     setcompany(newcompany);
  };

  return (
    <>
      <div className='company'>
        <div className='hiii'>
          <ul className='company_list'>
            <li>
              <div className='title_company'>
                <p>Car Brands</p>
              </div>
            </li>
            <li>
              <a href='brand/add'>Add New</a>
            </li>
          </ul>
        </div>

        <div>
          <TableContainer>
            <Table>
              <TableHead className='column'>
                <TableRow>
                  <TableCell>Index</TableCell>
                  <TableCell>Company_Id</TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Images</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {company.map((company,i) => {
                  return (
                    <TableRow>
                      <TableCell>{i+1}</TableCell>
                      <TableCell>{company.carcompany_id}</TableCell>
                      <TableCell>{company.car_companyName}</TableCell>
                      <TableCell>{company.status}</TableCell>
                      <TableCell>{company.date}</TableCell>
                      <TableCell>
                        <img src={company.car_companyLogo} alt='not Allowed' height='100px' width='100px'></img>
                      </TableCell>
                      <TableCell><IconButton>
                          <Link to={`/brand/edit/${company.carcompany_id}`}><EditIcon /></Link>
                      </IconButton></TableCell>
                      <TableCell><IconButton onClick={(e) => handleDelete(company.carcompany_id,)}>
                          <DeleteIcon />
                      </IconButton></TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    </>
  );
};

export default Company;
