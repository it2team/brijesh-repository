import "./Category.css";
import axios from "axios";
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { IconButton } from "@mui/material";
// import { Link } from '@material-ui/core';
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
// import PreviewIcon from "@mui/icons-material/Preview";
const Category = () => {
  const [category, setCategory] = useState([]);

  useEffect(() => {
    async function getDetails() {
      try {
        const category = await axios.get(
          `http://103.254.172.218:4200/carcategory`
        );
        setCategory(category.data);
      } catch (error) {
        console.log("Something is Wrong");
      }
    }
    getDetails();
  }, []);

  const handleDelete = async (id) => {
    await axios.delete("http://103.254.172.218:4200/carcategory", {
      data: { cat_id: id },
    });
    var newcategory = category.filter((item) => {
      // console.log(item);
      return item.id !== id;
     })
     setCategory(newcategory);
  };
  return (
    <>
      <div className='category'>
        <ul className='category_list'>
          <li>
            <div className='title_category'>
              <p>Category</p>
            </div>
          </li>
          <li>
            <Link to={`/category/add`}>Add New</Link>
          </li>
        </ul>
        <div>
          <TableContainer>
            <Table>
              <TableHead className='column'>
                <TableRow>
                  <TableCell>
                    <span>CategoryId</span>
                  </TableCell>
                  <TableCell>
                    <span>CategoryName</span>
                  </TableCell>
                  <TableCell>
                    <span>Date</span>
                  </TableCell>
                  <TableCell>
                    <span>Status</span>
                  </TableCell>
                  <TableCell>
                    <span>Action</span>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {category.map((item, i) => {
                  return (
                    <TableRow key={i}>
                      <TableCell>{item.cat_id}</TableCell>
                      <TableCell>{item.category_name}</TableCell>
                      <TableCell>{item.date}</TableCell>
                      <TableCell>{item.status}</TableCell>
                      <div>

                        <TableCell>
                          <IconButton>
                            <Link to={`/category/edit/${item.cat_id}`}>
                              <EditIcon />
                            </Link>
                          </IconButton>
                        </TableCell>
                        <TableCell>
                          <IconButton
                            onClick={(e) => handleDelete(item.cat_id, e)}>
                            <DeleteIcon />
                          </IconButton>
                        </TableCell>
                      </div>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    </>
  );
};

export default Category;

