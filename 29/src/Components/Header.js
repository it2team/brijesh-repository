import '../App.css';
const Header = () =>{
    return(<> 
        <div className="home-section"> 
            <nav>
                <div className="sidebar-button">
                    <span className="dashboard">Dashboard</span>
                </div>
                <div className="search-box">
                    <input type="text" placeholder="Search..."></input>
                </div>
                <div className="profile-details">
                    <span className="admin_name">Brijesh Italiya</span>
                </div>

            </nav>

        </div>
</>)}
export default Header;