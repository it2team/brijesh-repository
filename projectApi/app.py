from flask import Flask,jsonify
from flask import *
from flask_restful import Api
from flask_cors import CORS
from db import mysql
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims,get_jwt_identity
)



from resources.userResources import*

app=Flask(__name__)
CORS(app)
api=Api(app)



app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'infozium'
app.config['MYSQL_DATABASE_DB'] = 'Project'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/')
def get():
    return jsonify([{'message: "Please enter correct URL'}])


api.add_resource(UserList,'/user')
api.add_resource(userLogin,'/login')
api.add_resource(AddCar,'/addcar')
api.add_resource(carCategory,'/carcategory')
api.add_resource(carCompany,'/carcompany')



if __name__=="__main__":
    app.run(host="192.168.0.105", port=5000, debug=True, threaded=True)