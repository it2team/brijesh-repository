from requests.api import request
from models import userModel
# from app import*

import os
from flask import Flask, flash, request, redirect, url_for
import werkzeug
from werkzeug.utils import secure_filename
from flask_restful import Resource,reqparse

class UserList(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('Full_name',type=str,required=True,help="Full_name is required")
        parser.add_argument('user_name',type=str,required=True,help="user_name is required")
        parser.add_argument('user_password',type=str,required=True,help="company_address is required")
        parser.add_argument('email',type=str,required=True,help="email is required")
        parser.add_argument('Type',type=str,required=True,help="Type is required")
        parser.add_argument('contact_number',type=str,required=True,help="contact_number is required")
        parser.add_argument('address',type=str,required=False,help="address is required")
        parser.add_argument('landmark',type=str,required=False,help="landmark is required")
        parser.add_argument('establish_year',type=str,required=False,help="establish_year is required")

        # parser.add_argument('Password',type=str,required=True,help="Password is required")
        args = parser.parse_args()
        data=userModel.userModel.userInsert(args)
        if data:
            return data
        else:
            return {"message":"There is some problem"},500

class userLogin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_name',type=str,required=False,help="user_name  is required")
        parser.add_argument('email',type=str,required=False,help="email  is required")
        parser.add_argument('user_password',type=str,required=True,help="user_password is required")
        args = parser.parse_args()
        data=userModel.userModel.userLogin(args)
        if data:
            return data
        else:
            return {"message":"There is some Problem"},500


class AddCar(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('carmela_id',type=str,required=True,help="carmela_id is required")
        parser.add_argument('company_name',type=str,required=True,help="company_name is required")
        parser.add_argument('car_category',type=str,required=True,help="car_category is required")
        parser.add_argument('price',type=str,required=True,help="price is required")
        parser.add_argument('car_mileage',type=str,required=True,help="car_mileage is required")
        parser.add_argument('engine_type',type=str,required=True,help="engine_type is required")
        parser.add_argument('transmission',type=str,required=True,help="transmission is required")
        parser.add_argument('fuel_type',type=str,required=True,help="fuel_type is required")
        parser.add_argument('seater',type=str,required=True,help="seater is required")
        parser.add_argument('colour',type=str,required=True,help="colour is required")
        
        parser.add_argument('image', type=werkzeug.datastructures.FileStorage, location='files')
        args = parser.parse_args()
        print("args",args)
        image_file = args['image']
        print('\n\nimage_file',image_file)
        print(image_file.filename)
        # UPLOAD_FOLDER = '/home/huckleberry/Vivek/vivek-repository/projectApi/file/carImages/'
        filename = secure_filename(image_file.filename)
        image_file.save(os.path.join('./file/carImages',filename))
        data=userModel.userModel.addCar(args)
        if data:
            return data
        else:
            return {"message":"There is some problem"},500
                 
    def get(self):
        parser = reqparse.RequestParser()
        args = parser.parse_args()
        data=userModel.userModel.carList(args)
        if data:
            return data
        else:
            return {"message":"There is some Problem"},500

class carCategory(Resource):              
    def get(self):
        parser = reqparse.RequestParser()
        args = parser.parse_args()
        data=userModel.userModel.car_categoryList(args)
        if data:
            return data
        else:
            return {"message":"There is some Problem"},500

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('category_name',type=str,required=True,help="car_category is required")
        # parser.add_argument('category_name',type=str,required=True,help="car_category is required")
        # # parser.add_argument('date',type=str,required=True,help="car_category is required")
        args = parser.parse_args()
        data=userModel.userModel.car_categoryAdd(args)
        if data:
            return data
        else:
            return {"message":"There is some Problem"},500

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('cat_id',type=str,required=True,help="cat_id is required")
        parser.add_argument('category_name',type=str,required=True,help="car_category is required")
        args = parser.parse_args()
        data=userModel.userModel.car_categoryUpdate(args)
        if data:
            return data
        else:
            return {"message":"There is some Problem"},500

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('cat_id',type=str,required=True,help="cat_id is required")
        # parser.add_argument('Password',type=str,required=True,help="Password is required")
        args = parser.parse_args()
        data=userModel.userModel.car_categoryDelete(args)
        if data:
            return data
        else:
            return {"message":"There is some problem"},500


class carCompany(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('car_companyName',type=str,required=True,help="car_companyName is required")
        # parser.add_argument('car_companyLogo',type=str,required=True,help="car_companyLogo is required")
        # parser.add_argument('date',type=str,required=True,help="car_category is required")
        parser.add_argument('car_companyLogo', type=werkzeug.datastructures.FileStorage, location='files')
        args = parser.parse_args()
        print("args",args)
        image_file = args['car_companyLogo']
        print('\n\nimage_file',image_file)
        print(image_file.filename)
        # UPLOAD_FOLDER = '/home/huckleberry/Vivek/vivek-repository/projectApi/file/carImages/'
        filename = secure_filename(image_file.filename)
        image_file.save(os.path.join('./file/carCompanyLogo',filename))
        data=userModel.userModel.addCarCompany(args)
        if data:
            return data
        else:
            return {"message":"There is some problem"},500

    

    

    