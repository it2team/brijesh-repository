import sys

from pymysql import cursors

from db import mysql
import pymysql
from flask import jsonify
import re
import datetime
    
class userModel:
    @classmethod
    def userInsert(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            sql = """ SELECT * FROM user where user_name = %s AND email= %s """
            data = (args["user_name"], args["email"])
            cursor.execute(sql,data)
            rows = cursor.fetchall()
            # print('rows',rows)
            # cursor.close()
            # name=cursor.customer_name
            if (rows):
                res = jsonify({'messgae':'User allready there Please try another one'})
                res.status_code = 404
                return res

           
            sql = """ INSERT INTO user (Full_name,user_name, user_password,email,Type,contact_number,address,landmark,establish_year) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s); """
           
            data = (args["Full_name"],args["user_name"],args["user_password"],args["email"],args["Type"],args["contact_number"],args["address"],args["landmark"],args["establish_year"])
            
            cursor.execute(sql,data)
            # cursor.execute(sql1,data1)
            con.commit()

            id=cursor.lastrowid
            print("id",id)





            # id =args["emp_id"]
            cursor.close()
            con.close()
            

            lst = []
       
            res = jsonify({'message':'USER Details Is Added Successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500


    @classmethod
    def userLogin(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
            if (regex):
                sql = """ SELECT * FROM user where email = %s AND user_password= %s """
                data = (args["email"], args["user_password"])

        
            sql = """ SELECT * FROM user where user_name = %s AND user_password = %s """
            data = (args["user_name"],args["user_password"])

            cursor.execute(sql,data)
            rows = cursor.fetchall()
    
            cursor.close()
            con.close()
            if not rows:
                res = jsonify({'messgae':'Please Enter Correct UserId/Email or Password'})
                res.status_code = 404
                return res
            res = jsonify(rows,{'message':'login succesfully'})
    
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500



    @classmethod
    def addCar(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)

            sql = """ INSERT INTO AddCar (carmela_id,company_name,car_category,price,
                      car_mileage,engine_type,transmission,fuel_type,seater,colour,image) 
                      VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s); """
           
            data = (args["carmela_id"],args["company_name"],args["car_category"],args["price"],
                    args["car_mileage"],args["engine_type"],args["transmission"],args["fuel_type"],
                    args["seater"],args["colour"],args["image"])
            
            cursor.execute(sql,data)
            # cursor.execute(sql1,data1)
            con.commit()
            # id =args["emp_id"]
            cursor.close()
            con.close()
            lst = []
            res = jsonify({'message':'Car Details Is Added Successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500

    @classmethod
    def carList(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            sql = """ SELECT * FROM AddCar """
            
            cursor.execute(sql)

            rows = cursor.fetchall()
            cursor.close()
            con.close()



            res = jsonify(rows)
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500

    @classmethod
    def car_categoryList(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            sql = """ SELECT * FROM car_category """
            
            cursor.execute(sql)

            rows = cursor.fetchall()
            cursor.close()
            con.close()



            res = jsonify(rows)
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500

    
    @classmethod
    def car_categoryAdd(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            x=datetime.datetime.now()

            sql = """ INSERT INTO car_category (category_name,status,date) VALUES (%s,'1',%s) """
           
            data = (args["category_name"],x)
            
            cursor.execute(sql,data)
            con.commit()
        
            cursor.close()
            con.close()
            lst = []
            res = jsonify({'message':'Car Category Is Added Successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500


    @classmethod
    def car_categoryUpdate(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            x=datetime.datetime.now()
            sql = """ UPDATE car_category SET category_name = %s WHERE cat_id = %s """
            data = (args["category_name"],args["cat_id"])
            # print("sql",sql)
            cursor.execute(sql,data)
            # print("data",data)
            con.commit()
            cursor.close()
            con.close()
            

            lst = []
       
            res = jsonify({'message':'Car Category  updated successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500

    @classmethod
    def car_categoryDelete(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            sql = """ DELETE FROM  car_category WHERE cat_id = %s """
            data = (args["cat_id"])
            cursor.execute(sql,data)
            # print("data",data)
            con.commit()
            cursor.close()
            con.close()
       
            res = jsonify({'message':'Car Category is deleted successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500



    
    @classmethod
    def addCarCompany(self, args):
        try:
            con = mysql.connect()
            cursor = con.cursor(pymysql.cursors.DictCursor)
            x=datetime.datetime.now()

            sql = """ INSERT INTO  car_company (car_companyName,status,car_companyLogo,date	) 
                      VALUES (%s,'1',%s,%s); """
           
            data = (args["car_companyName"],args["car_companyLogo"],x)
            
            
            cursor.execute(sql,data)
            # cursor.execute(sql1,data1)
            con.commit()
            # id =args["emp_id"]
            cursor.close()
            con.close()
            lst = []
            res = jsonify({'message':'Car Company Details Is Added Successfully'})
            res.status_code = 200
            return res

        except Exception as e:
            print("err", e)
            return {"message": "Error in Api"}, 500
            


    
    
