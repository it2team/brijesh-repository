import { Link } from "react-router-dom"; 
import './Car.css';
const CarAdd= () =>{
    return(
        <>
            <div className='car'>
                <ul className='car_list'>
                    <li>
                        <div className="title_car">
                            <p>Add new Car</p>
                        </div>
                    </li>
                    <li>
                        <Link to='/car'>Back To Home</Link>
                    </li>
                </ul>
            </div>

            <div className='car_add_for'>
                <form>
                    <div className='form'>
                        <p>Enter the Car</p>
                        <input type='name'></input>
                    </div>
                </form>
            </div>

        </>
    )
}
export default CarAdd;