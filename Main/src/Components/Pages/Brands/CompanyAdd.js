import { useState } from "react";
import axios from "axios";
import "./Company.css";
import Files from "react-files";
import { Link } from "react-router-dom";
const CompanyAdd = () => {
  const [name,setName] = useState('Set Brand Logo')
  const [company, setCompany] = useState('')
  const [logo, setLogo] = useState();
  const [image,setImage] = useState();
  // const [name, setname] = useState('')
  const changeHandler = (e) => {
    setCompany(e.target.value)
  };

  const uploadHandler = (e) => {
    // console.log(e[0].preview.url)
    setLogo(e[0])
    setName(e[0].name)
    setImage(e[0].preview.url)
  };
  async function onSubmit(e) {
    e.preventDefault();
    const fd = new FormData();
    fd.append('car_companyName',company)
    fd.append("car_companyLogo",logo)
    try {
      await axios.post(`http://103.254.172.218:4200/carcompany`,fd);
      console.log("success");
    } catch (error) {
      console.log("Something is Wrong");
    }
  }

  return (
    <>
          <div className='company'>
        <ul className='company_list'>
          <li>
            <div className='title_company'>
              <p>Add New Brand</p>
            </div>
          </li>
          <li>
            <Link to='/brand'>Back TO Home</Link>
          </li> 
        </ul>
      </div>
      <div className='carcompany_add_form'>
        <form>
          <div className='form'>
            <p>Enter the Brand Name</p>
            <input
              type='name'
              onChange={(e) => changeHandler(e)}
              placeholder='Enter the Brand Name'
              name='car_companyName'></input>
            {/* <p>Enter the Date</p>
            <input
              type='date'
              onChange={(e) => changeHandler(e)}
              placeholder='ENter the Date'
              name='date'></input>
            <p>Choose the Status</p>
            <select name='status' value='1' onChange={changeHandler}>
              <option label='Active' value='1'>
                Active
              </option>
              <option label='Off' value='0'>
                Off
              </option>
            </select> */}
            <p>Upload Logo</p>
            <div className='logo'>
              <Files
                onChange={(e) => uploadHandler(e)}
                accepts={["image/*"]}
                maxFiles={1}
                name='car_companyLogo'
                maxFileSize={10000000}
                minFileSize={0}
                clickable>
                <p className="upd">{name}</p>
              </Files>
            </div>
            
            <div className="img">
              <img src={image} alt="Wait" height='100px' width='100px'></img>
            </div>
            {/* <input
              type='file'
              alt='Notfound'
              accept='.jpg'
              id='logo'
              onChange={(e) => textChangeHandler(e)}
              placeholder='Upload Image'
              name='car_companyLogo'></input>
            <hr /> */}

            <button onClick={(e) => onSubmit(e)} className='but_cta'>
              Submit
            </button>
            <hr />
          </div>
        </form>
      </div>

    </>
  );
};

export default CompanyAdd;
