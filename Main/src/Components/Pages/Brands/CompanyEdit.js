import axios from "axios";
import "./Company.css";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Files from "react-files";
const CompanyEdit = () => {
  const { id } = useParams();
  const [logo,setLogo] = useState();
  const [name,setName] = useState('Set Brand Logo')
  // const [companyname,setCompanyName] = useState();
  const [company, setCompany] = useState({
    car_companyName: "",
    car_companyLogo:"",
    date: "",
    status: "",
  });

  useEffect(() => {
    setLogo(company.car_companyLogo) 

    getDetails(id);
  }, [id,company.car_companyLogo])


  //for fetch the value from company api for display on form initially
  async function getDetails(id) {
    axios
      .get(`http://103.254.172.218:4200/singlecarcompany?carcompany_id=${id}`)
      .then((res) => {
        setCompany(res.data[0]);
        // console.log(res.data[0])  
      })
      .catch(() => {
        console.log("Something is Wrong");
      });
     
  }


  // //Logo
  const uploadHandler = (e) => {

    setLogo(e[0])
    setName(e[0].name)

  };

  // //for the name change handling

  const changeHandler = (e) => {
      setCompany(e.target.value)
  };


  // for post the updated data into the database
  async function onSubmit(e) {
    e.preventDefault();
    const fd = new FormData();
    // fd.append('car_companyName',companyname)
    console.log(company)
    console.log(logo)
    fd.append('car_companyName',company)
    fd.append('car_companyLogo',logo)
    try {
      await axios.put(
        `http://103.254.172.218:4200/carcompany?carcompany_id=${id}`,fd
      );
      console.log("success")
    } catch (error) {
      console.log("Something is Wrong");
    }
  }

  return (
    <>
      {/* Title Code */}
      <div className='company'>
        <ul className='company_list'>
          <li>
            <div className='title_company'>
              <p>Update Brand</p>
            </div>
          </li>
          <li>
            <Link to='/brand'>Back TO Home</Link>
          </li>
        </ul>
      </div>

    {/* Edit Form Code */}
      <div className='carcompany_add_form'>
        <form>
          <div className='form'>
            <input type='hidden' name='cat_id' value={id}></input>

            <p>Update Brand Name </p>
            <input
              type='name'
              name='car_companyName'
              value={company.car_companyName}
              onChange={changeHandler}
            >
              </input>

            <p>Update Brand Logo Here</p>

            <div className='logo'>
              <Files
                onChange={(e) => uploadHandler(e)}
                accepts={["image/*"]}
                maxFiles={1}
                name='car_companyLogo'
                maxFileSize={10000000}
                minFileSize={0}
                clickable>
                <p className='upd'>{name}</p>
              </Files>
            </div>

            <div className='img'>
              <img  src={logo} alt='Wait' height='100px' width='100px'></img>
            </div>

            <br />
            <button className='but_cta' onClick={onSubmit}>
              Submit
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default CompanyEdit;