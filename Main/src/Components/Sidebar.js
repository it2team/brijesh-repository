import '../App.css';
import { Link } from 'react-router-dom'; 
const Sidebar = () =>{  
    return(    
    <>      
    <div className="sidebar">     
        <div className="logo-details">      
                <span className="logo_name">Car Becho</span>     
        </div>      
        <ul className="nav-links">       
            <li>    
            <Link to={`/brand`}><span className="links_name">Brands</span></Link>          
            </li>       
             <li>        
           <Link to={`/category`}><span className="links_name">Category</span></Link>    
            </li>  
            <li>        
           <Link to={`/car`}><span className="links_name">Car</span></Link>    
            </li>
            <li>        
           <Link to={`/customer`}><span className="links_name">Customer</span></Link>    
            </li>
            <li>        
           <Link to={`/carmelaowner`}><span className="links_name">Car mela Owner list</span></Link>    
            </li> 
            <li>        
           <Link to={`/testdrive`}><span className="links_name">Test Drive </span></Link>    
            </li> 
            <li>        
           <Link to={`/#`}><span className="links_name">Log Out </span></Link>    
            </li>
        </ul>    
    </div>    
    </>  
)}

export default Sidebar;