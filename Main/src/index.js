import React from 'react';
import ReactDOM from 'react-dom';
// import TestDrive from './Components/Pages/Testdrive/TestDrive';
import App from './App';
// import CategoryEdit from './Components/Pages/Category/Categoryedit';
// import AdminLogin from './Components/AdminLogin';
import { BrowserRouter} from 'react-router-dom';
// import Images from './Components/Images'
// import Demo from './Components/Pages/Brands/Demo'
ReactDOM.render(
  <BrowserRouter><App /></BrowserRouter>,document.getElementById('root')
);
