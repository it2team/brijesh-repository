// import ComponentA from './ComponentA';
// import ComponentB from './ComponentB';

// import { Routes, Route, Link} from 'react-router-dom';

// const App = () =>{
//   return(
//     <>
//     <p>Hello</p>
//     <Route path="./componentA">
//       <ComponentA />
//     </Route>
//     <Route path="./componentB">
//       <ComponentB />
//     </Route>  
//     </>
//   );
// };

// export default App;

import { Link } from "react-router-dom";
import { Route } from "react-router-dom";
export default function App() {
  return (
    <div>
      <h1>Bookkeeper</h1>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem"
        }}
      >
        <Link to="/invoices"><ComponentA /></Link> |{" "}
        <Link to="/expenses"><ComponentB /></Link>
      </nav>
    </div>
  );
}